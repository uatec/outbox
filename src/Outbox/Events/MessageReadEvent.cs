using System;

namespace Outbox.Events
{
	public class MessageReadEvent
	{
		public MessageReadEvent()
		{
			this.DateTime = DateTime.Now;
		}

		// Core event properties. This stuff is essential
		public string MessageId { get; set; }
		public DateTime DateTime { get; set; }
	}
}

