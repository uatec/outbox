using System;

namespace Outbox.Events
{
	public class MessageDoneEvent
	{
		public MessageDoneEvent()
		{
			this.DateTime = DateTime.Now;
		}

		// Core event properties. This stuff is essential
		public string MessageId { get; set; }
		public DateTime DateTime { get; set; }
	}
}

