using System;
using Outbox.Models;
using System.Collections.Generic;

namespace Outbox.Events
{
	public class MessageReceivedEvent
	{
		public MessageReceivedEvent()
		{
			this.DateTime = DateTime.Now;
		}

		// Core event properties. This stuff is essential
		public string MessageId { get; set; }
		public DateTime DateTime { get; set; }
	}

	public class MessageReceivedEventDetails
	{
		public MessageReceivedEventDetails()
		{
			this.DateTime = DateTime.Now;
		}

		// Core event properties. This stuff is essential
		public string MessageId { get; set; }
		public string Id { get { return this.MessageId; } }
		public DateTime DateTime { get; set; }

		// All this stuff is just informational, it can be looked up given the information above
		public List<Attachment> Attachments { get; set; }
        public Address From { get; set; }
		/// <summary>
		/// Bundle All To fields and BCC etc in to here.
		/// </summary>
		public List<Address> To { get; set; }
		public List<Address> CC { get; set; }


		public DateTime Received { get; set; }
		public string RawContentUrl { get; set; }
		public string Body { get; set; } 
		public string ConversationId { get; set; }
		public string Subject { get; set; }
		public bool Draft { get; set; }
		public bool IsRead { get; set; } 
		public string ReplyTo { get; set;  }
	}
}

