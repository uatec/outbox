﻿using Microsoft.AspNet.Mvc;
using Microsoft.Exchange.WebServices.Data;
using Outbox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Outbox.Controllers
{

	[OutboxSessionExists]
	public class OutboxController : EWSController
	{
		public IActionResult Index(string id)
		{
			ViewBag.UserName = Encoding.UTF8.GetString(Context.Session["username"]).ToLower();

			return View((object)id.ToLower());
		}
		public IActionResult NewUI()
		{
			ViewBag.UserName = Encoding.UTF8.GetString(Context.Session["username"]).ToLower();

			return View();
		}
		public IActionResult Query(string id, int offset = 0, int count = 10)
		{
			var view = new ItemView(count, offset);
			
            var items =	base.ExchangeService.FindItems(base.ExchangeService.GetFolderId("Done"), id, view);
				
				
			var conversations = items
				.Cast<EmailMessage>()
				.GroupBy(i => i.ConversationId)
				.Select(g => new Models.Conversation
				{
					UnreadCount = g.Count(m => !m.IsRead),
					Id = g.Key,
					Subject = g.First().Subject,
					LatestDate = g.Max(m => m.DateTimeReceived),
					MessageIds = g.Select(m => m.Id.UniqueId).ToList()
				}).ToList();

			return Json(conversations);
		}

		public IActionResult Get(string id, int offset = 0, int count = 100)
		{
			FolderId folderId = null;
			switch (id.ToLower())
			{
				case "outbox":
					folderId = WellKnownFolderName.Inbox;
					break;
				case "done":
					folderId = base.ExchangeService.GetFolderId("Done");
					break;
				case "sent":
					folderId = WellKnownFolderName.SentItems;
					break;
				case "drafts":
					folderId = WellKnownFolderName.Drafts;
					break;

			}
            return Json(GetConversations(folderId, count, offset));
		}

		private Message translateMessage(EmailMessage i)
		{
			return new Models.Message
			{
				Id = i.Id.UniqueId,
				Subject = i.Subject,
				RawContentUrl = "/message/read?messageIdentifier=" + Uri.EscapeDataString(i.Id.UniqueId),
				To = i.ToRecipients.Select(to => new Address(to)).ToList(),
				CC = i.CcRecipients.Select(cc => new Address(cc)).ToList(),
				From = i.IsDraft && i.From == null ? new Address { Name = "Me" } : new Address(i.From),	// if it's a draft and has no from field yet, just say it's from me. It's my draft, right?
				ConversationId = i.ConversationId.UniqueId,
				Draft = i.IsDraft,
				IsRead = i.IsRead,
				Received = i.DateTimeReceived,
				Attachments = i.Attachments.Where(x => !x.IsInline).Select(x => new Models.Attachment
				{
					Name = x.Name,
					Id = x.Id,
					Url = "/message/attachment?messageIdentifier=" + Uri.EscapeDataString(i.Id.UniqueId) + "&contentId=" + Uri.EscapeDataString(x.Id)
                }).ToList()

			};
        }
		private List<Models.Conversation> GetConversations(FolderId folderId, int count, int offset)
		{
			// Get conversations we're interested in

			ConversationIndexedItemView ciiv = new ConversationIndexedItemView(count, offset);
			ICollection<Microsoft.Exchange.WebServices.Data.Conversation> conversations = base.ExchangeService.FindConversation(ciiv, folderId);


			//EmailMessage message = EmailMessage.Bind(base.ExchangeService, new ItemId(messageIdentifier));
			//message.Load(BasePropertySet.FirstClassProperties);

			return conversations
				.Select(g => new Models.Conversation
				{
					UnreadCount = g.UnreadCount,
					Id = g.Id.UniqueId,
					Subject = g.Topic,
					LatestDate = g.LastDeliveryTime,
					MessageIds = g.GlobalItemIds.Select(i => i.UniqueId).ToList(),
				}).ToList();

		}

		//private List<Message> GetMessages(FolderId folderId, int count, int offset)
		//{




		//	// As a best practice, limit the properties returned to only those that are required.
		//	// In this scenario, you only need the FolderId.
		//	PropertySet propSet = new PropertySet(BasePropertySet.FirstClassProperties, new[] { ItemSchema.Subject, ItemSchema.DisplayTo, ItemSchema.Body });

		//	// Bind to an existing folder and get only the properties specified in the PropertySet.
		//	// This method call results in a GetFolder call to EWS.
		//	Folder rootfolder = Folder.Bind(ExchangeService, folderId, propSet);

		//	var items = rootfolder.FindItems(new ItemView(count, offset));
		//	if (!items.Any())
		//	{
		//		return new List<Message>();
		//	}

		//	ExchangeService.LoadPropertiesForItems(items, PropertySet.FirstClassProperties);

		//	var messages = items.Items.Cast<EmailMessage>().Select(i => new Message
		//	{
		//		Id = i.Id.UniqueId,
		//		Subject = i.Subject,
		//		RawContentUrl = "/message/read?messageIdentifier=" + i.Id.UniqueId,
		//		To = i.DisplayTo,
		//		From = i.From.Name,
		//		ConversationId = i.ConversationId.UniqueId
		//	})
		//	.ToList();
		//	return messages;
		//}
	}
}
