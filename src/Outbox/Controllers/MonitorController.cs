using Microsoft.AspNet.Mvc;
using Outbox.Data;

namespace Outbox.Controllers
{
	public class MonitorController : Controller
	{
		private readonly ISessionManager _sessionManager;
		private readonly IRequestManager _requestManager;

		public MonitorController(ISessionManager sessionManager,
			IRequestManager requestManager)
		{
			_sessionManager = sessionManager;
			_requestManager = requestManager;
		}

		public IActionResult Sessions()
		{
			return Json(_sessionManager.GetAll());
		}

		public IActionResult Requests()
		{
			var requests = _requestManager.GetAll();
			return Json(new {
						Count = requests.Count,
						Requests = requests
					});
		}
	}
}