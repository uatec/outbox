﻿using Microsoft.AspNet.Mvc;
using Microsoft.Exchange.WebServices.Data;
using System;
using System.Text;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Outbox.Controllers
{
	public abstract class EWSController : Controller
	{
		protected string UserName 
		{ 
			get 
			{
				byte[] usernameBytes = new byte[20];
				if (!Context.Session.TryGetValue("username", out usernameBytes))
				{
					throw new Exception("not authenticated");
				}
				return Encoding.UTF8.GetString(usernameBytes);		
			}
		}
		
		protected ExchangeService ExchangeService
		{
			get
			{
                return ServiceCache.Get(this.UserName);
			}
		}
	}	
}
