﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Outbox.Controllers
{
	
	public class OutboxSessionExistsAttribute : ActionFilterAttribute, IAuthorizationFilter
	{
		public void OnAuthorization(AuthorizationContext context)
		{
			try
			{
				var service = ServiceCache.Get(Encoding.UTF8.GetString(context.HttpContext.Session["username"]));
			}
			catch (Exception)
			{
				context.Result = new RedirectToActionResult("Login", "Session", new Dictionary<string, object>
				{
					{ "returnUrl", context.HttpContext.Request.Path + context.HttpContext.Request.QueryString }
				});
			}
		}
	}

	
}
