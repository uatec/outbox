﻿using Microsoft.AspNet.Mvc;
using Microsoft.Exchange.WebServices.Data;
using Outbox.Models;
using System;
using System.Linq;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Outbox.Controllers
{
	

	[OutboxSessionExists]
	public class CalendarController : EWSController
	{
		public IActionResult Get(DateTime start, DateTime end)
		{
			var calendarView = new CalendarView(start, end);
			var calendarEntries = base.ExchangeService.FindAppointments(WellKnownFolderName.Calendar, calendarView)
				.Select(x => new CalendarEntry
				{
					id = x.Id.UniqueId,
					content = x.Subject ?? "<blank>",
					Location = x.Location,
					start = x.Start,
					end = x.End
				});
			return Json(calendarEntries);
		}
	}

}
