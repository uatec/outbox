﻿using Microsoft.AspNet.Mvc;
using Microsoft.Exchange.WebServices.Data;
using Outbox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Outbox.Controllers
{
	[OutboxSessionExists]
	public class MessageController : EWSController
	{
		private Regex _urlReplace = new Regex("http[s]?:\\/\\/[^\\s]*");
		private Message translateMessage(EmailMessage i)
		{
			
			string messageBody = i.Body.Text.Replace("src=\"cid:",
					string.Format("src=\"http://localhost:5004/message/inlineattachment?messageIdentifier={0}&contentId=", Uri.EscapeDataString(i.Id.UniqueId)));
			
			if ( i.Body.BodyType == BodyType.Text)
			{
				messageBody = _urlReplace.Replace(messageBody, "<a href='$&' target=\"_new\">$&</a>");
				messageBody = messageBody.Replace(Environment.NewLine, "<br />");
				// there should be no \r\ns left, so do them one at a time for stand alones.
				messageBody = messageBody.Replace("\r", "<br />");
				messageBody = messageBody.Replace("\n", "<br />");
				messageBody = "<html><body>" + messageBody + "</body></html>";
			}
			return new Models.Message
			{
				Type = MessageType.Message,
				Id = i.Id.UniqueId,
				Subject = i.Subject,
				Body = messageBody,
				To = i.ToRecipients.Select(to => new Address(to)).ToList(),
				CC = i.CcRecipients.Select(cc => new Address(cc)).ToList(),
				From = i.IsDraft && i.From == null ? new Address { Name = "Me" } : new Address(i.From),	// if it's a draft and has no from field yet, just say it's from me. It's my draft, right?
				ConversationId = i.ConversationId.UniqueId,
				Draft = i.IsDraft,
				IsRead = i.IsRead,
				Received = i.DateTimeReceived,
				MimeType = "text/html; charset=UTF-8",
				Attachments = i.Attachments.Where(x => !x.IsInline).Select(x => new Models.Attachment
				{
					Name = x.Name,
					Id = x.Id,
					Url = "/message/attachment?messageIdentifier=" + Uri.EscapeDataString(i.Id.UniqueId) + "&contentId=" + Uri.EscapeDataString(x.Id)
                }).ToList()

			};
        }
		
		private Message translateMessage(Microsoft.Exchange.WebServices.Data.MeetingRequest i)
		{
			
			string messageBody = i.Body.Text.Replace("src=\"cid:",
					string.Format("src=\"http://localhost:5004/message/inlineattachment?messageIdentifier={0}&contentId=", Uri.EscapeDataString(i.Id.UniqueId)));
			
			if ( i.Body.BodyType == BodyType.Text)
			{
				messageBody = _urlReplace.Replace(messageBody, "<a href='$&' target=\"_new\">$&</a>");
				messageBody = messageBody.Replace(Environment.NewLine, "<br />");
				// there should be no \r\ns left, so do them one at a time for stand alones.
				messageBody = messageBody.Replace("\r", "<br />");
				messageBody = messageBody.Replace("\n", "<br />");
				messageBody = "<html><body>" + messageBody + "</body></html>";
			}
			return new Models.MeetingRequest
			{
				Type = MessageType.MeetingRequest,
				Id = i.Id.UniqueId,
				Subject = i.Subject,
				Body = messageBody,
				To = i.ToRecipients.Select(to => new Address(to)).ToList(),
				CC = i.CcRecipients.Select(cc => new Address(cc)).ToList(),
				From = i.IsDraft && i.From == null ? new Address { Name = "Me" } : new Address(i.From),	// if it's a draft and has no from field yet, just say it's from me. It's my draft, right?
				ConversationId = i.ConversationId.UniqueId,
				Draft = i.IsDraft,
				IsRead = i.IsRead,
				Received = i.DateTimeReceived,
				MimeType = "text/html; charset=UTF-8",
				Attachments = i.Attachments.Where(x => !x.IsInline).Select(x => new Models.Attachment
				{
					Name = x.Name,
					Id = x.Id,
					Url = "/message/attachment?messageIdentifier=" + Uri.EscapeDataString(i.Id.UniqueId) + "&contentId=" + Uri.EscapeDataString(x.Id)
                }).ToList(),
				Start = i.Start,
				End = i.End,
				Location = i.Location

			};
        }
		
		public IActionResult Load(string id)
		{
			id = Uri.UnescapeDataString(id);
			id = Uri.UnescapeDataString(id);
			id = Uri.UnescapeDataString(id);
			id = Uri.UnescapeDataString(id);
			Console.WriteLine("Loading message: {0}", id);
			var message = EmailMessage.Bind(base.ExchangeService, id);
			message.Load(new PropertySet(BasePropertySet.FirstClassProperties, 
				EmailMessageSchema.IsRead,
				EmailMessageSchema.From,
				EmailMessageSchema.ToRecipients,
				EmailMessageSchema.CcRecipients,
				EmailMessageSchema.IsDraft,
				EmailMessageSchema.Attachments,
				EmailMessageSchema.Body
				));
				Console.WriteLine("loaded and translating {0}", message.GetType().Name);
			if (  message is Microsoft.Exchange.WebServices.Data.MeetingRequest )
			{
				
				Models.Message responseMessage = translateMessage((Microsoft.Exchange.WebServices.Data.MeetingRequest) message);
				Console.WriteLine("Done loading message: {0}", id);
				return Json(responseMessage);
			} 
			else if (  message is Microsoft.Exchange.WebServices.Data.MeetingResponse )
			{
				throw new Exception("not supported displaying meeting responses");
			}
			else 
			{
				Models.Message responseMessage = translateMessage(message);
				Console.WriteLine("Done loading message: {0}", id);
				return Json(responseMessage);
			}
		}
		
		[HttpPost]
		public IActionResult SendMessage([FromBody] Message message)
		{
			if (message.ReplyTo != null)
			{
				EmailMessage replyTo = EmailMessage.Bind(base.ExchangeService, message.ReplyTo);
				ResponseMessage reply = replyTo.CreateReply(true);


				reply.ToRecipients.AddRange(message.To.Select(e => e.Email));
				if (message.CC != null)
				{
					reply.CcRecipients.AddRange(message.CC.Select(e => e.Email));
				}
				reply.Subject = message.Subject;
				reply.BodyPrefix = message.Body;
				reply.SendAndSaveCopy();
				return Content("ok");

			}

			EmailMessage emailMessage = new EmailMessage(this.ExchangeService);

			emailMessage.ToRecipients.AddRange(message.To.Select(e => e.Email));
			if (message.CC != null)
			{
				emailMessage.CcRecipients.AddRange(message.CC.Select(e => e.Email));
			}
			emailMessage.Subject = message.Subject;
			emailMessage.Body = message.Body;
			emailMessage.SendAndSaveCopy();
			return Content("ok");	
		}

		[HttpPost]
		public IActionResult Delete(string messageIdentifier)
		{
			EmailMessage emailMessage = EmailMessage.Bind(base.ExchangeService, messageIdentifier);
			emailMessage.Delete(DeleteMode.MoveToDeletedItems);

			return Content("ok");
		}

		[HttpPost]
		public IActionResult MarkAsRead(string conversationId)
		{
			EmailMessage emailMessage = EmailMessage.Bind(base.ExchangeService, conversationId,
				new PropertySet(BasePropertySet.IdOnly));
			emailMessage.IsRead = true;
			emailMessage.Update(ConflictResolutionMode.AutoResolve);

			return Content("ok");
		}

		[HttpPost]
		public IActionResult MarkAsDone(string conversationId)
		{
            base.ExchangeService.MoveItemsInConversations(
				new[] {new KeyValuePair<ConversationId, DateTime?>(conversationId, DateTime.MaxValue)},
				WellKnownFolderName.Inbox,
				base.ExchangeService.GetFolderId("Done"));
			base.ExchangeService.SetReadStateForItemsInConversations(new[] { new KeyValuePair<ConversationId, DateTime?>(conversationId, DateTime.MaxValue) }, base.ExchangeService.GetFolderId("Done"), true);

			return Content("ok");
		}
		
		[HttpPost]
		public IActionResult Postpone(string conversationId, string postponeTill)
		{
			string[] validTimes = new []{ "This Afternoon", "Tonight", "Tomorrow"};
			if ( !validTimes.Contains(postponeTill)) 
			{
				Response.StatusCode = 400;
				return Json(new {
					Field = "postponeTill",
					Error = "Value not allowed.",
					Options = validTimes,
					ValuePassed = postponeTill
				});
			}
			
            base.ExchangeService.MoveItemsInConversations(
				new[] {new KeyValuePair<ConversationId, DateTime?>(conversationId, DateTime.MaxValue)},
				WellKnownFolderName.Inbox,
				base.ExchangeService.GetFolderId(buildFolderName(postponeTill, DateTime.Now)));
			base.ExchangeService.SetReadStateForItemsInConversations(new[] { new KeyValuePair<ConversationId, DateTime?>(conversationId, DateTime.MaxValue) }, base.ExchangeService.GetFolderId("Done"), true);
	
			return Content("ok");
		}
		
		private static string buildFolderName(string folderName, DateTime now)
		{
			switch ( folderName)
			{
				
				case "Tomorrow":
					var dt1 = new DateTime(now.Year, 
						now.Month,
						now.Day,
						9, 0, 0);
					dt1 = dt1.AddDays(1);
					return "Postponed Till " + dt1.ToString("o");
				case "Tonight":
					var dt2 = new DateTime(now.Year, 
						now.Month,
						now.Day,
						18, 0, 0);
					if ( now > dt2 )
					{
						dt2 = dt2.AddDays(1);
					}
					return "Postponed Till " + dt2.ToString("o");
					
				case "This Afternoon":
					var dt = new DateTime(now.Year, 
						now.Month,
						now.Day,
						12, 0, 0);
					if ( now > dt )
					{
						dt = dt.AddDays(1);
					}
					return "Postponed Till " + dt.ToString("o");
				default:
					throw new Exception(folderName  + " is not a known time to postpone items until");
			}
		}
			

		public IActionResult InlineAttachment(string messageIdentifier, string contentId)
		{
			EmailMessage message = EmailMessage.Bind(base.ExchangeService, new ItemId(messageIdentifier));
			message.Load(new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Attachments));
			FileAttachment chosenAttachment = message.Attachments.OfType<FileAttachment>().Single(a => a.ContentId.Equals(contentId));
			chosenAttachment.Load();
			return File(chosenAttachment.Content, chosenAttachment.ContentType, chosenAttachment.FileName);
		}

		public IActionResult Attachment(string messageIdentifier, string contentId)
		{
			EmailMessage message = EmailMessage.Bind(base.ExchangeService, new ItemId(messageIdentifier));
			message.Load(new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Attachments));
			FileAttachment chosenAttachment = message.Attachments.OfType<FileAttachment>().Single(a => a.Id.Equals(contentId));
			chosenAttachment.Load();
			return File(chosenAttachment.Content, chosenAttachment.ContentType, chosenAttachment.Name);
		}

		private Regex _contentRegex = new Regex("\"(?<url>cid:[^\"]*)\"");
		public IActionResult Read(string messageIdentifier)
		{
			try
			{
				EmailMessage message = EmailMessage.Bind(base.ExchangeService, new ItemId(messageIdentifier));
				message.Load(new PropertySet(BasePropertySet.FirstClassProperties, EmailMessageSchema.Attachments));

				string messageBody = message.Body.Text;

				messageBody = messageBody.Replace("src=\"cid:",
					string.Format("src=\"/message/inlineattachment?messageIdentifier={0}&contentId=", Uri.EscapeDataString(messageIdentifier)));


				return Content(messageBody, "text/html");
			}
			catch (Exception ex)
			{
				switch (ex.Message)
				{
                    case "Id is malformed.":
						Response.StatusCode = 500;
						return Content(ex.Message);
					case "The specified object was not found in the store.":
						return HttpNotFound();
                    default:
						throw;
				}
			}
		}
	}
	
	
}
