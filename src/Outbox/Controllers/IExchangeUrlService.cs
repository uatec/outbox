using System;

namespace Outbox.Controllers
{
	public interface IExchangeUrlService
	{
		Uri Get(string email, string password);
	}
}