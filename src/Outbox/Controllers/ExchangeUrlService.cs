using System;
using Microsoft.Exchange.WebServices.Data;

namespace Outbox.Controllers
{
	public class ExchangeUrlService : IExchangeUrlService
	{
		public Uri Get(string email, string password)
		{
			ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
			service.Credentials = new WebCredentials(email, password);
			Console.WriteLine("AutodiscoverUrl");
			service.EnableScpLookup = false;
			service.AutodiscoverUrl(email, a => true);
			Console.WriteLine("url discovered");
			return service.Url;
		}
	}
}