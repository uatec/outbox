﻿using Microsoft.Exchange.WebServices.Data;
using System.Collections.Concurrent;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Outbox.Controllers
{
	

	public static class ServiceCache
	{
		public static ExchangeService Get(string username)
		{
			return Instance[username];
		}

		public static void Set(string username, ExchangeService instance)
		{
			Instance[username] = instance;
		}
		private static ConcurrentDictionary<string, ExchangeService> Instance = new ConcurrentDictionary<string, ExchangeService>();
	}
	
}
