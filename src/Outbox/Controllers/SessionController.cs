﻿using Microsoft.AspNet.Mvc;
using Microsoft.Exchange.WebServices.Data;
using Outbox.Models;
using System;
using System.Text;
using Microsoft.Framework.ConfigurationModel;
using Newtonsoft.Json;
using Outbox.Events;
using System.Linq;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Outbox.Controllers
{

	public class SessionController : Controller
	{
		private readonly IConfiguration _configuration;
		private readonly IBus _bus;

		public SessionController(IConfiguration configuration,
			IBus bus)
		{
			_configuration = configuration;
			_bus = bus;
		}

		[HttpGet]
		public IActionResult Logout()
		{
			Context.Session["username"] = null;
			return RedirectToAction("Index", "Outbox");
		}

		[HttpGet]
		public IActionResult Login(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}

		[HttpPost, ActionName("Login")]
		public IActionResult LoginPost(LoginViewModel model, string ReturnUrl)
		{
			if (string.IsNullOrWhiteSpace(ReturnUrl) || !ReturnUrl.StartsWith("/"))
			{
				ModelState.AddModelError("", "Invalid return URL.");
				return View(model);
			}
			System.Console.WriteLine("discovering URL");


			var exchangeService = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
			
			exchangeService.Credentials = new WebCredentials(model.UserName, model.Password);

			IExchangeUrlService exchangeUrlService =
				new RedisCachingExchangeUrlService(new ExchangeUrlService(),
					_configuration.Get("session:redis:hostname"),
					_configuration.Get<int>("session:redis:port"));

			exchangeService.Url = exchangeUrlService.Get(model.UserName, model.Password);

			System.Console.WriteLine("Done");

			System.Console.WriteLine("binding inbox to validate credentials");
			// this will fail if the creds are wrong
			Folder folder = Folder.Bind(exchangeService, WellKnownFolderName.Inbox);

			System.Console.WriteLine("done");


			System.Console.WriteLine("caching exchange service");

			Context.Session["username"] = Encoding.UTF8.GetBytes(model.UserName);

			ServiceCache.Set(model.UserName, exchangeService);

			System.Console.WriteLine("done");


			Console.WriteLine("Establishing exchange subscription");

			var streamingSubscription = exchangeService.SubscribeToStreamingNotifications(
				new FolderId[] { WellKnownFolderName.Inbox },
					EventType.NewMail,
					EventType.Created,
					EventType.Modified
				);
  			
  			StreamingSubscriptionConnection connection = new StreamingSubscriptionConnection(exchangeService, 30); // keep connection open for 30 minutes. we'll need to schedule a reconnect after that, if appropriate
  			connection.AddSubscription(streamingSubscription);
  			connection.OnNotificationEvent += OnNotificationEvent;
  			connection.OnDisconnect += OnDisconnect;
  			connection.Open();

			Console.WriteLine("Done");
			
			Console.WriteLine("KickingOff postponed message manager");
			// This is REALLY nasty and hacky. will result in memory leaks and all sorts. 
			// But if it works, we can tidy it up.
			// TODO: SuperSerious 
			System.Threading.Tasks.Task.Run(() => 
			{
				while ( true )
				{
					try {						
						Console.WriteLine("checking for postponed items");
						foreach (Folder postponeFolder in exchangeService.FindFolders(WellKnownFolderName.Inbox, new FolderView(100)))
						{
							Console.WriteLine("considering '{0}' for de-postponing", postponeFolder.DisplayName);
							if ( isPostponedFolder(postponeFolder.DisplayName) )
							{ 
								Console.WriteLine("\tpostponed till: " + getPostponedDate(postponeFolder.DisplayName).ToString());
								Console.WriteLine("\tCurrent time: " + DateTime.Now.ToString());
							
								if ( getPostponedDate(postponeFolder.DisplayName) < DateTime.Now)
								{
									Console.WriteLine("postponed items '{0}' have become active", postponeFolder.DisplayName);
									exchangeService.MoveItems(postponeFolder.FindItems(new ItemView(100)).Select(i => i.Id), WellKnownFolderName.Inbox);
									postponeFolder.Delete(DeleteMode.HardDelete);
								}
							}
						} 
					}
					catch ( Exception ex)
					{
						Console.WriteLine(JsonConvert.SerializeObject(ex, Formatting.Indented));
					}
					System.Threading.Thread.Sleep(20000);
				}
			});

			System.Console.WriteLine("redirecting to returnurl");
			return Redirect(ReturnUrl);

		}
		
		private static bool isPostponedFolder(string folderName)
		{
			return folderName.StartsWith("Postponed Till");
		}
		
		private static DateTime getPostponedDate(string folderName)
		{
			Console.WriteLine("parsing folder name: {0}", folderName);
			folderName = folderName.Substring(16, folderName.Length - 17);
			return DateTime.Parse(folderName);			
		}

		private void OnNotificationEvent(object sender, NotificationEventArgs args)
		{
			foreach ( NotificationEvent notificationEvent in args.Events)
			{
				switch ( notificationEvent.EventType )
				{ 
					// folder has been modified to contain new mail
					// something has been created (the new mail)
					// A new mail has been received (the new mail)
					case EventType.NewMail:
						ItemEvent newMailEvent = (ItemEvent) notificationEvent;
						_bus.Publish(new MessageReceivedEvent {
								MessageId = newMailEvent.ItemId.UniqueId,
								DateTime = newMailEvent.TimeStamp
							});
						break;
					case EventType.Created:
						if ( notificationEvent is ItemEvent )
						{
							ItemEvent createdEvent = (ItemEvent) notificationEvent;
							_bus.Publish(new MessageReceivedEvent {
								MessageId = createdEvent.ItemId.UniqueId,
								DateTime = createdEvent.TimeStamp
							});	
						}
						// if this isn't an item event it's a new folder and we're not interested in folders. We mask them
						break;
					// case Eventtype.Modified:
						break;
					default:
						//Console.WriteLine("unhandled exchange subscription event");
						//Console.WriteLine(JsonConvert.SerializeObject(args));
						break;
				}
			}
		}

		private void OnDisconnect(object sender, SubscriptionErrorEventArgs args)
		{
			Console.WriteLine("ews stream disconnected - " + JsonConvert.SerializeObject(args));
            
            // Cast the sender as a StreamingSubscriptionConnection object.
            StreamingSubscriptionConnection connection = (StreamingSubscriptionConnection)sender;
            if (!connection.IsOpen)
            {
            	Console.WriteLine("ews stream reopenned");
                connection.Open();
            }
            else
            {
            	Console.WriteLine("ews stream still open after disconnect. wat?");
            }
		}
	}
}
