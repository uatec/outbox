using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.Exchange.WebServices.Data;
using Outbox.Models;

namespace Outbox.Controllers
{
	[OutboxSessionExists]
	public class ContactController : EWSController
	{
		public IActionResult Query(string query, int count = 100, int offset = 0)
		{
			//FindItemResults<Item> contacts = base.ExchangeService.FindItems(WellKnownFolderName.Contacts, query, new ItemView(count, offset));
			return Json(base.ExchangeService.ResolveName(query, ResolveNameSearchLocation.ContactsThenDirectory, true).Select(n => new Address
			{
				Name = n.Contact?.DisplayName,
				Email = n.Mailbox.Address
			}));
		}
	}
}