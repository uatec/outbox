using System;
using BookSleeve;

namespace Outbox.Controllers
{
	public class RedisCachingExchangeUrlService : IExchangeUrlService
	{
		private readonly IExchangeUrlService _service;
		private readonly RedisConnection _connection;
		private readonly object _syncRoot = new object();

		public RedisCachingExchangeUrlService(IExchangeUrlService service, string host, int port)
		{
			_service = service;
			_connection = new RedisConnection(host, port);
			_connection.Open();
		}

		public Uri Get(string email, string password)
		{
			try 
				{
				string url = _connection.Strings.GetString(0, email + ":" + password).Result;
				if (url == null)
				{
					lock (_syncRoot)
					{
						url = _connection.Strings.GetString(0, email + ":" + password).Result;
						if (url == null)
						{
							System.Console.WriteLine("exchange URL not found in redis: discovering");
							Uri discoveredUri = _service.Get(email, password);
							_connection.Strings.Set(0, email + ":" + password, discoveredUri.OriginalString);
							return discoveredUri;
						}
					}
				}
				System.Console.WriteLine("Exchange URL found in redis.");
				return new Uri(url);
			}
			catch (Exception ex)
			{
				if (ex.InnerException != null)
				{
					Console.WriteLine("Exception loading exchange URL from redis. delegating... (" + ex.InnerException.Message + ")");
				}
				else
				{
					Console.WriteLine("Exception loading exchange URL from redis. delegating... (" + ex.Message + ")");
				}

				return _service.Get(email, password);
			}
		}
	}
}