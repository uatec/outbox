﻿using System.Linq;
using Microsoft.Exchange.WebServices.Data;

namespace Outbox
{
    public static class ExchangeExtensions
    {
        public static FolderId GetFolderId(this ExchangeService self, string name)
        {

            FolderView fv = new FolderView(10);

            var findFoldersResults = self.FindFolders(
                WellKnownFolderName.Inbox,
                    new SearchFilter.IsEqualTo(FolderSchema.DisplayName, name),
                fv);

            if (findFoldersResults.Any())
            {
                return findFoldersResults.Single().Id;
            }

            Folder newFolder = new Folder(self);
            newFolder.DisplayName = name;
            newFolder.Save(WellKnownFolderName.Inbox);
            return newFolder.Id;
        }
    }
}
