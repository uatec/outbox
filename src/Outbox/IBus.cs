﻿using System;

namespace Outbox
{
    public interface IBus
    {
        void Publish<T>(T @event);
        void Subscribe<T>(Action<T> action);
    }

    public interface ISubscriber<T>
    {
        void Handle(T @event);
    }

    public static class BusExtensions
    {
        public static void Subscribe<T>(this IBus bus, ISubscriber<T> subscriber)
        {
            bus.Subscribe<T>(subscriber.Handle);
        }

        public static void Subscribe<T1, T2>(this IBus bus, ISubscriber<T2> subscriber)  where T1: T2
        {
            bus.Subscribe<T1>(o => subscriber.Handle((T2) o));
        }
    }
}
