using System;
using Outbox.Events;
using Microsoft.Exchange.WebServices.Data;
using System.Collections.Generic;

namespace Outbox.Subscribers
{
	public class PropagateDoneActivityToServer :
		ISubscriber<MessageDoneEvent>
	{
		private readonly ExchangeService _exchangeService;

		public PropagateDoneActivityToServer(ExchangeService exchangeService)
		{
			_exchangeService = exchangeService;
		}
		
		public void Handle(MessageDoneEvent @event)
		{
			Console.WriteLine("Marking converation as Done (moving to Done folder): "  + @event.MessageId);
			_exchangeService.MoveItemsInConversations(
				new[] {new KeyValuePair<ConversationId, DateTime?>(@event.MessageId, DateTime.MaxValue)},
				WellKnownFolderName.Inbox,
				_exchangeService.GetFolderId("Done"));

			// Todo : decide if we really want to mark a message as read, just because we've marked it as done. it's not necessary and might be hiding unnecessary information
			_exchangeService.SetReadStateForItemsInConversations(new[] { new KeyValuePair<ConversationId, DateTime?>(@event.MessageId, DateTime.MaxValue) }, _exchangeService.GetFolderId("Done"), true);
		}
	}
}