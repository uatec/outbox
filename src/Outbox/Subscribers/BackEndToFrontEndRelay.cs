using Microsoft.AspNet.SignalR;

namespace Outbox.Subscribers
{
	public class BackEndToFrontEndRelay :
		ISubscriber<object>
	{
		private readonly IHubContext _hubContext;

		public BackEndToFrontEndRelay(IHubContext hubContext)
		{
			_hubContext = hubContext;
		}

		public void Handle(object @event)
		{
			_hubContext.Clients.All.RelayEventToClient(new {
				Type = @event.GetType().Name,
				Data = @event
				});
		}
	}
}