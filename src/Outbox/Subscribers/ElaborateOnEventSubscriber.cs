using System.Linq;
using System;
using Outbox.Events;
using Outbox.Models;
using Microsoft.Exchange.WebServices.Data;
using Outbox.Controllers;

namespace Outbox.Subscribers
{
	public class ElaborateOnEventSubscriber :
		ISubscriber<MessageReceivedEvent>
	{
		private readonly IBus _bus;

		public ElaborateOnEventSubscriber(IBus bus)
		{
			_bus = bus;
		}

		public void Handle(MessageReceivedEvent @event)
		{

			var exchangeService = ServiceCache.Get("robert.stiff@Easyjet.com");

			var message = EmailMessage.Bind(exchangeService, @event.MessageId);
				message.Load(new PropertySet(BasePropertySet.FirstClassProperties, 
					EmailMessageSchema.IsRead,
					EmailMessageSchema.From,
					EmailMessageSchema.ToRecipients,
					EmailMessageSchema.CcRecipients,
					EmailMessageSchema.IsDraft,
					EmailMessageSchema.Attachments,
					EmailMessageSchema.Subject
					));
			Console.WriteLine("message elaborated on and republished");


			_bus.Publish(translateMessage(message));
		}

		private MessageReceivedEventDetails translateMessage(EmailMessage i)
		{
			return new MessageReceivedEventDetails
			{
				MessageId = i.Id.UniqueId,
				Subject = i.Subject,
				RawContentUrl = "/message/read?messageIdentifier=" + Uri.EscapeDataString(i.Id.UniqueId),
				To = i.ToRecipients.Select(to => new Address(to)).ToList(),
				CC = i.CcRecipients.Select(cc => new Address(cc)).ToList(),
				From = i.IsDraft && i.From == null ? new Address { Name = "Me" } : new Address(i.From),	// if it's a draft and has no from field yet, just say it's from me. It's my draft, right?
				ConversationId = i.ConversationId.UniqueId,
				Draft = i.IsDraft,
				IsRead = i.IsRead,
				Received = i.DateTimeReceived,
				Attachments = i.Attachments.Where(x => !x.IsInline).Select(x => new Models.Attachment
				{
					Name = x.Name,
					Id = x.Id,
					Url = "/message/attachment?messageIdentifier=" + Uri.EscapeDataString(i.Id.UniqueId) + "&contentId=" + Uri.EscapeDataString(x.Id)
	            }).ToList()

			};
	    }
	}
}