using Outbox.Events;
using Microsoft.Exchange.WebServices.Data;

namespace Outbox.Subscribers
{
	public class PropagateReadActivityToServer :
		ISubscriber<MessageReadEvent>
	{
		private readonly ExchangeService _exchangeService;

		public PropagateReadActivityToServer(ExchangeService exchangeService)
		{
			_exchangeService = exchangeService;
		}
		
		public void Handle(MessageReadEvent @event)
		{
			EmailMessage emailMessage = EmailMessage.Bind(_exchangeService, @event.MessageId,
				new PropertySet(BasePropertySet.IdOnly));
			emailMessage.IsRead = true;
			emailMessage.Update(ConflictResolutionMode.AutoResolve);
		}
	}
}