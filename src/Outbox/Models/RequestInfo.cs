using System;

namespace Outbox.Models 
{
	public class RequestInfo
	{
		public string RequestId { get; set; }
		public string Url { get; set; }
		public string Method { get; set; }
		public string Controller { get; set; }
		public string Action { get; set; }
		public string SessionId { get; set; }
		public string ClientAddress { get; set;}
		public DateTime RequestStarted { get; set; }
	}
}