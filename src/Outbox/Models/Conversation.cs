using System;
using System.Collections.Generic;

namespace Outbox.Models
{
	public class Conversation
	{
		public string Id { get; set; } 
		public string Subject { get; set; }
		private IList<string> _messagesIds = new List<string>();
		public IList<string> MessageIds { get { return _messagesIds; } set { _messagesIds = value; } }
		public DateTime LatestDate { get; set; }
		public int UnreadCount { get; set; }
	}
}