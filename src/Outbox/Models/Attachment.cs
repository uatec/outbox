﻿namespace Outbox.Models
{
	public class Attachment
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public string Url { get; set; }
	}
}