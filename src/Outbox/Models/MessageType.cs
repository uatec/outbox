namespace Outbox.Models
{
	public enum MessageType
	{
		Message,
		MeetingRequest,
		MeetingResponse
	}
}