﻿using System;

namespace Outbox.Models
{
	public class CalendarEntry
	{
		public string id { get; set; }
		public DateTime start { get; set; }
		public DateTime end { get; set; }
		public string content { get; set; }
		public string Location { get; set; }
	}
}