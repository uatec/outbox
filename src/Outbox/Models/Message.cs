﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Outbox.Models
{
	
	public class Message
	{
		[JsonConverter(typeof(StringEnumConverter))]
		public MessageType Type { get; set; }
		public List<Attachment> Attachments { get; set; }
        public Address From { get; set; }
		/// <summary>
		/// Bundle All To fields and BCC etc in to here.
		/// </summary>
		public List<Address> To { get; set; }
		public List<Address> CC { get; set; }


		public DateTime Received { get; set; }
		public string Id { get; set; }
		public string RawContentUrl { get; set; }
		public string Body { get; set; } 
		public string ConversationId { get; set; }
		public string Subject { get; set; }
		public bool Draft { get; set; }
		public bool IsRead { get; set; } 
		public string ReplyTo { get; set;  }
		public string MimeType { get; set; }
	}
	
	public class MeetingRequest : Message
	{
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public string Location { get; set; }	
	}
}
