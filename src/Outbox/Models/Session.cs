using System;

namespace Outbox.Models 
{
	public class Session
	{
		public string SessionId { get; set; }
		public string ClientHostName { get; set; }
		public string UserId { get; set; }
		public DateTime SessionStarted { get; set; }
		public DateTime LastActive { get; set; }
	}
}