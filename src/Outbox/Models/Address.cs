using Microsoft.Exchange.WebServices.Data;

namespace Outbox.Models
{
	public class Address
	{
		private string _name;
		private string _email;

		public Address()
		{
		}

		public Address(EmailAddress emailAddress)
		{
			this.Name = emailAddress.Name;
			this.Email = emailAddress.Address.ToLower();
		}

		public string Name
		{
			get
			{
				return _name ?? _email;
			}
			set { _name = value; }
		}
		public string Email
		{
			get
			{
				return _email ?? _name;
			}
			set
			{
				_email = value;
			}
		}
	}
}