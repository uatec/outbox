using System;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using Outbox.Subscribers;
using Outbox.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Outbox.Hubs
{
	[HubName("announcement")]
	public class AnnouncementHub : Hub
	{
		private readonly IBus _bus;

		public AnnouncementHub(IBus bus, IConnectionManager connectionManager)
		{
			_bus = bus;
			var context = connectionManager.GetHubContext<AnnouncementHub>();
			_bus.Subscribe<MessageReceivedEventDetails, object>(new BackEndToFrontEndRelay(context));
			_bus.Subscribe<MessageReadEvent, object>(new BackEndToFrontEndRelay(context));
			_bus.Subscribe(new PropagateDoneActivityToServer(Controllers.ServiceCache.Get("robert.stiff@Easyjet.com")));
			_bus.Subscribe(new PropagateReadActivityToServer(Controllers.ServiceCache.Get("robert.stiff@Easyjet.com")));
			Console.WriteLine("hub created and relay subscribed");
		}

		public void RelayEventToClient(object eventObject)
		{
			Clients.All.relayevent(
				new {
					Type = eventObject.GetType().Name, 
					Data = eventObject
					});
			Console.WriteLine("event relayed from server to client");
		}

		public void RelayEventToApplication(string objString)
		{
			Console.WriteLine("Message received from client: " + objString);
			JObject jobj = JObject.Parse(objString);
			object obj = null;
			switch ( (string) jobj["Type"] )
			{
				case "MessageReadEvent":
					obj = JsonConvert.DeserializeObject<MessageReadEvent>(jobj["Data"].ToString());
					break;
				case "MessageDoneEvent":
					obj = JsonConvert.DeserializeObject<MessageDoneEvent>(jobj["Data"].ToString());
					break;
				default:
					Console.WriteLine("Unhandled message received from client: " + objString);
					break;
			}
			if ( obj != null )
			{
				_bus.Publish(obj);
			}
		}
	}
}