﻿using System;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Diagnostics;
using Microsoft.AspNet.Diagnostics.Entity;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.ConfigurationModel;
using Microsoft.Framework.DependencyInjection;
using Microsoft.Framework.Logging;
using Microsoft.Framework.Logging.Console;
using System.Text;
using Outbox.Data;
using Outbox.Subscribers;

namespace Outbox
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Setup configuration sources.
            Configuration = new Configuration()
                .AddJsonFile("config.json")
                .AddEnvironmentVariables();

            SessionManager = new MemorySessionManager();
            RequestManager = new MemoryRequestManager();

            Bus = new MemoryBus();
            // Bus.Subscribe<object>(ev => 
            //     {
            //         Console.WriteLine(new { 
            //             Type = ev.GetType().Name, 
            //             Data = JsonConvert.SerializeObject(ev)
            //             });
            //     });
            Bus.Subscribe(new ElaborateOnEventSubscriber(Bus));
        }

        public IBus Bus { get; set; }

        public IRequestManager RequestManager { get; set; }
        public ISessionManager SessionManager { get; set; }
        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add MVC services to the services container.
            services.AddMvc();
            services.AddSignalR();

			services.AddCaching();
			services.AddSession();


			services.AddInstance(this.Configuration);
            services.AddInstance(this.SessionManager);
            services.AddInstance(this.RequestManager);

            services.AddInstance(this.Bus);
            // services.AddTransient<BackEndToFrontEndRelay, BackEndToFrontEndRelay>();
		}

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerfactory)
        {
			app.UseSession(o => {
				o.IdleTimeout = TimeSpan.FromDays(1);
			});

			// Configure the HTTP request pipeline.
			// Add the console logger.
			loggerfactory.AddConsole();

            // Add the following to the request pipeline only in development environment.
            if (string.Equals(env.EnvironmentName, "Development", StringComparison.OrdinalIgnoreCase))
            {
                app.UseBrowserLink();
                app.UseErrorPage(ErrorPageOptions.ShowAll);
                app.UseDatabaseErrorPage(DatabaseErrorPageOptions.ShowAll);
            }
            else
            {
                // Add Error handling middleware which catches all application specific errors and
                // send the request to the following path or controller action.
                app.UseErrorHandler("/Home/Error");
            }

            // Add static files to the request pipeline.
            app.UseStaticFiles();
            app.Use(next => async ctx =>
            {
                if ( ctx.Request.Cookies[".AspNet.Session"] != null )
                {
                    byte[] binaryUserName = ctx.Session["username"];
                    string userName = binaryUserName != null ? Encoding.UTF8.GetString(binaryUserName) : null;

                    //var connectionFeature = ctx
                     //          .GetFeature<Microsoft.AspNet.Http.IHttpConnectionFeature>();

                    //string ip = connectionFeature.RemoteIpAddress.ToString();
                    string ip = "not found yet";
                    this.SessionManager.RecordSessionActivity(
                        sessionId: ctx.Request.Cookies[".AspNet.Session"],
                        clientHostName: ip,
                        userId: userName);
                }
                await next(ctx);
                
            });

            app.Use(next => async ctx => 
            {
                string requestId = Guid.NewGuid().ToString();

                this.RequestManager.RecordRequestStarting(
                    DateTime.Now,
                    requestId, 
                    ctx.Request.Path.Value, 
                    ctx.Request.Method, 
                    "unknown", 
                    "unknown", 
                    ctx.Request.Cookies[".AspNet.Session"], 
                    "unknown");

                await next(ctx);

                this.RequestManager.RecordRequestEnded(requestId);

            });


            // Add MVC to the request pipeline.
            app.UseMvc(routes =>
            {

				routes.MapRoute(
					name: "other",
					template: "{controller}/{action}/{id?}",
					defaults: new { controller = "Outbox", action = "Index", id = "Outbox"});

				// Uncomment the following line to add a route for porting Web API 2 controllers.
				// routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");
			});

            app.UseSignalR();

           
        }
    }
}
