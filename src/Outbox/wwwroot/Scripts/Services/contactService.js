﻿(function () {
    'use strict';

    var outboxServices = angular.module('outboxServices');

    outboxServices.factory('Contact', ['$resource',
	    function($resource) {
		    return $resource('/contact/query', {}, {
				query: { method: 'GET', isArray: true}
		    });
	    }]);
})();