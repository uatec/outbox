﻿(function () {
    'use strict';

    var outboxServices = angular.module('outboxServices');

    outboxServices.factory('Events', [
    	'Hub',
    	function(Hub)
    	{
    		var context = {
    			eventHandlers: {}
    		};

    		var hub = new Hub("Announcement", {

    			listeners: {
    				'relayeventtoclient': function(ev)
    				{
    					if ( ev.Type in context.eventHandlers)
    					{
    						context.eventHandlers[ev.Type](ev.Data);    						
    					}
    					else
    					{
    						console.log({
    							message: 'unhandled event', 
                                type: ev.Type,
    							ev: ev
    						});
    					}
    				}
    			},
    			methods: ['RelayEventToApplication'],
    			errorHandler: function(error) {
    				console.error(error);
	   			}
    		});
    		hub.connect();
            context.relayeventtosapplication = function(ev) { hub.RelayEventToApplication(JSON.stringify(ev)); };
    		return context;

    	}]);
})();