﻿(function () {
    'use strict';
    var outboxServices = angular.module('outboxServices', ['ngResource', 'SignalR']);

    outboxServices.factory('Calendar', [
		'$resource',
		function ($resource) {
			return $resource('/calendar/get', {}, {
				get: { method: 'GET', isArray: true }
			});
		}]);
})();