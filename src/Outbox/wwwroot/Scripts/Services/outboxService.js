﻿(function () {
    'use strict';

    var outboxServices = angular.module('outboxServices');

    outboxServices.factory('Outbox', ['$resource',
		function ($resource) {
			return $resource('/outbox/:verb/:term', {}, {
				get: { method: 'GET', isArray: true, params:{verb: 'get', term: '@mailbox'} },
				search: { method: 'GET', isArray: true, params:{verb: 'query'} }
			});
		}]);
})();