﻿(function () {
    'use strict';

    var outboxServices = angular.module('outboxServices');

    outboxServices.factory('Message', ['$resource',
		function ($resource) {
			return $resource('/message/:verb/:id', {}, {
				get: {method: 'GET', params: {verb:'load', id: '@id'} },
				markasdone: { method: 'POST', params: { verb: 'markasdone', conversationId: '@conversationId'} },
				send: { method: 'POST', params: { verb: 'sendmessage' } },
				markasread: { method: 'POST', params: { verb: 'markasread', conversationId: '@messageIdentifier' } },
				delete: { method: 'GET', params: { verb: 'delete'}},
				postpone: {method: 'POST', params: { verb: 'postpone', conversationId: '@conversationId', postponeTill: '@postponeTill'}}
			});
		}]);
})();