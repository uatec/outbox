﻿(function () {
    'use strict';

    angular.module('outboxApp', [
        // Angular modules 
        'ui.bootstrap',// for accordion
		//'oi.multiselect',  // for To Field
		'ngTagsInput',
		'textAngular', // for email editor,
		'ngSanitize', // to sanitize the content of emails before rendering
		'ngVis', // calendar display
		'dcbClearInput', // the clear input button for the search box
        // Custom modules 
		'outboxServices'
        // 3rd Party Modules
        
    ]);
})();