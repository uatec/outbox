function calendarController($scope, Calendar, VisDataSet)
{
	    $scope.calendar = { items: new VisDataSet() };
	    var start = new moment().subtract(1, 'month');
	    var end = new moment().add(1, 'month');
    	Calendar.get({ start: start.format(), end: end.format() })
		    .$promise.then(function(calendarEntries) {
		    	calendarEntries.forEach(function(ce) {
		    		if ( !"content" in ce)
		    		{
		    			ce.content = "<blank>";
		    		}
		    	});
		    $scope.calendar.items.add(calendarEntries);
	    });
}


    angular
		.module('outboxApp')
		.controller('calendarController', calendarController);
