﻿(function () {
    'use strict';
		
	Array.prototype.unique = function()
	{
		var distinctKeys = {};
		var distinctObjects = [];
		
		var keySelector = function(v) { return v; };
		if ( arguments.length > 0 )
		{
			keySelector = arguments[0];
		}	
		
		this.forEach(function(v) {
			var key = keySelector(v);
			if ( !distinctKeys[key] )
			{
				distinctKeys[key] = true;
				distinctObjects.push(v);
			}
		});
		
		return distinctObjects;
	};
		
    Array.prototype.remove = function(predicate)
	{
		for ( var i = 0; i < this.length; i++ )
		{
			if ( predicate(this[i]))
			{
				this.splice(i, 1);
				i--; // we just removed an element, go back so we can try this slot again			
			}
		}
	};


    angular
        .module('outboxApp')
        .controller('outboxController', outboxController);

    outboxController.$inject = ['$scope', '$timeout', 'Outbox', 'Message', 'Contact', '$http', 'Events', '$sce'];

    function arrayContains(array, instance, comparitor) { // i think i made up the word comparitor
    	for (var i = 0; i < array.length; i++) {
		    if (comparitor(array[i], instance))
			    return true;
	    }
	    return false;
    }

    function outboxController($scope, $timeout, Outbox, Message, ContactService, $http, Events , $sce) {
    	$scope.hub = Events;
		
		$scope.Me = new Contact({
			Name: "Me",
			Email: window.myEmail
		});

    	$scope.hub.eventHandlers["MessageReceivedEventDetails"] = function(ev)
    	{
    		var conversations = $scope.Views['outbox'].filter(function(c) { return c.Id == ev.ConversationId});
    		var conversation = new Conversation({
    				Id : ev.ConversationId,
    				Subject: ev.Subject,
					MessageIds: [ev.MessageId],
					UnreadCount: 1,
					LatestDate: ev.ReceivedDate
			});
			if ( conversations.length == 0 )
    		{
    			$scope.Views['outbox'].push(conversation);
    		}
    		else
    		{
    			conversation = conversations[0];
    		}
    	};

	    $scope.SearchText = "";
		var searchTimeout;
		var runSearch = function(searchTerm)
		{
			var resultsView = Outbox.search({ term: searchTerm })
			    .$promise
			    .then(function (results) {
			    	// hack all results to appear 'not done' so that they are displayed.
			    	// really there should be a way to suppress the hiding of 'done' conversations for pages that don't concern them selves like that
					var conversationResults = [];
					results.forEach(function(c) {
						var conv = new Conversation(c);
			    		conv.IsDone = false;
						conversationResults.push(conv);
					});
			    	$scope.Views['search'] = conversationResults
				    $scope.currentViewName = "search";				
			    });
		};
	    $scope.$watch('SearchText', function (newValue, oldValue) {
		    if (newValue != null && newValue.length > 3) {
			    if ( searchTimeout != null)
				{
					clearTimeout(searchTimeout);
					searchTimeout = null;
				}
				searchTimeout = setTimeout(function() {
					runSearch(newValue);
				}, 1000);
		    } else {
			    $scope.currentViewName = "outbox";
		    }
			});


	    $scope.Loading = true;

		$scope.LoadConversation = function(conversation)
		{
			if ( !conversation.AllMessagesLoaded() )
			{
				conversation.State = "loading";
				conversation.MessageIds.forEach(function(id){
					if ( conversation.Messages[id] == undefined )
					{
						Message.get({ id: id }).$promise
							.then(function (a) {
								a.Body = "data:" + a.MimeType  + "," + encodeURIComponent(a.Body);
								a.Body = $sce.trustAsResourceUrl(a.Body);
								var message = new Message(a, conversation);
								message.IsOpen = message.IsOpen || !message.IsRead;
								conversation.AddMessage(message);
								$scope.MarkAsRead(message, conversation);
								if ( conversation.AllMessagesLoaded() )
								{
									conversation.State = "ready";
								}
						    });
					}
				});
			}
		};
		

		$scope.queryContacts = function(query) {
			return ContactService.query({ query: query }).$promise.then(function(r) {
				var results = [];
				r.forEach(function(v) {
					results.push(new Contact(v));
				});
				return results;
			});
		};
	    $scope.currentViewName = "outbox";
		$scope.currentView = function() {
			return $scope.Views[$scope.currentViewName];
		}
		$scope.Views = {
			outbox: [],
			done: [],
			sent: [],
			drafts: [],
			search: [],
	    };
	    // setUpdateTimeout();
	    $scope.NewMessage = function () {
			// TODO: these should be honoured in the constructor.
		    var conversation = new Conversation({
		    });
			conversation.State = "ready";
			conversation.IsOpen = true;
			
			var newMessage = {
				Id: guid(),
	    		Draft: true,
	    		IsOpen: true,
	    		IsRead: true,
	    		From: $scope.Me,
	    		Subject: "",
	    		Body: "",
	    		To: [],
	    		CC: [],
				ReplyTo: null
	    	};
			conversation.MessageIds.push(newMessage.Id);
	    	conversation.AddMessage(newMessage);
			
		    $scope.currentView().push(conversation);
	    };
		
		function guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
			}
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			s4() + '-' + s4() + s4() + s4();
		}
		
	    $scope.Reply = function (conversation, message) {
			// Remove ME from the To list, given that i'm now in the from
	    	var messageTo = [message.From].concat(message.To);
	    	for (var i = 0; i < messageTo.length; i++) {
	    		if (messageTo[i].Email == window.myEmail) {
	    			messageTo.splice(i, 1);
			    }
	    	}

	    	// Remove ME from the cc list, given that i'm now in the from
	    	var messageCC = [].concat(message.CC); // copy not reference, we don't want to change the original message's CC fields
	    	for (var i = 0; i < messageCC.length; i++) {
	    		if (messageCC[i].Email == window.myEmail) {
	    			messageCC.splice(i, 1);
	    		}
	    	}

//	    	messageTo = uniqBy(messageTo, function (a) { return a.Email; });
//	    	messageCC = uniqBy(messageCC, function (a) { return a.Email; });

			var newMessage = {
				Id: guid(),
	    		Draft: true,
	    		IsOpen: true,
	    		IsRead: true,
	    		From: $scope.Me,
	    		Subject: Replyify(conversation.Subject),
	    		Body: "",
	    		To: messageTo,
	    		CC: messageCC,
				ReplyTo: message.Id,
	    	};
			conversation.MessageIds.push(newMessage.Id);
	    	conversation.AddMessage(newMessage);
		    return false;
	    };

	    var Replyify = function (subject) {

	    	var isFwdedAlready = subject.toUpperCase().substring(0, 3) === "FW:";
	    	var isRepliedAlready = subject.toUpperCase().substring(0, 3) === "RE:";
	    	if (isRepliedAlready) {
			    return subject;
	    	} else if (isFwdedAlready) {
			    subject = subject.substring(4);
	    	}
		    return "Re: " + subject;
	    };

		var contactizeRecipient = function(c)
		{
			if (c.__proto__ === Contact)
			{
				return c;
			}
			else
			{
				return new Contact(c);
			}
		};
		
		var contactizeRecipientList = function(rawRecipients)
		{
			var contactizedRecipients = [];
			rawRecipients.forEach(function(c) {
				contactizedRecipients.push(contactizeRecipient(c));
			});
			return contactizedRecipients;
		};
		
	    $scope.Send = function(message) {
			
			message.To = contactizeRecipientList(message.To);
			message.CC = contactizeRecipientList(message.CC);
			
		    Message.send(message)
	    	.$promise.then(function (a) {
	    		//message.Draft = false;
	    		var parent = message.GetParent();
	    		if (parent.Subject == null || parent.Subject == '') {
				    parent.Subject = message.Subject;
	    		}
			    message.Draft = false;
			    message.IsOpen = false;
			    message.ReceivedDate = new moment();
				
				// If this is the only message in this conversation, it must be a new conversation only created for the new message
				// so we can consider it done.
				if (parent.MessageIds.length == 1)
				{
					parent.State = "done";
				}
		    });
	    };

	    function UnreadMessagesInConversation(conversation) {
	    	var unreadCount = 0;
	    	for (var m = 0; m < conversation.Messages.length; m++) {
	    		var message = conversation.Messages[m];
	    		if (!message.IsRead && !message.Draft) {
	    			unreadCount++;
	    		}
	    	}
		    return unreadCount;
	    }

	    $scope.UnreadMessages = function () {
	    	var unreadCount = 0;
	    	for (var c = 0; c < $scope.Views['outbox'].length; c++) {
	    		var convo = $scope.Views['outbox'][c];
	    		convo.unreadCount = UnreadMessagesInConversation(convo);
	    		unreadCount += convo.unreadCount;
		    }
		    return unreadCount;
	    };

		if ( typeof(Favico) != 'undefined')
		{
	    	// i can't think of anywhere else to put this
		    var favicon = new Favico({
				animation: 'slide'
		    });
		    if (window.Notification) {
			    Notification.requestPermission(function(status) {
				    $scope.$watch('UnreadMessages()', function(newValue, oldValue) {
					    if (newValue > 0) {
						    favicon.badge(newValue); // up date the badge thing
					    } else {
						    favicon.reset();
					    }
	
					    if (newValue > oldValue) {
						    var n = new Notification("New Message", { body: "You have " + newValue + " unread messages." });
						    n.onshow = function() {
							    setTimeout(n.close.bind(n), 5000);
						    }
					    }
				    });
			    });
		    }
		}


	    $scope.Delete = function (conversation, message) {
		    var deleteFrontEnd = function(a) {
			    for (var i = 0; i < conversation.Messages.length; i++) {
				    if (conversation.Messages[i] == message) {
					    conversation.Messages.splice(i, 1);
				    }
			    }
		    };
		    if (message['Id'] != undefined) {
			    Message.delete({ messageIdentifier: message.Id })
				    .$promise.then(deleteFrontEnd);
		    } else {
			    deleteFrontEnd();
		    }
	    };
		
		$scope.Postpone = function (conversation, postponeTill)
		{
			Message.postpone({
				conversationId: conversation.Id, 
				postponeTill: postponeTill})
			.$promise.then(function(a){
				$scope.currentView().remove(function(c) { return c.Id == conversation.Id; });
			});
		};

	    $scope.MarkAsRead = function (message, conversation) {
	    	if (message.Id == null) { // if this message doesn't have an id. because it's a draft or something?
			    return;
		    }
		    // $scope.hub.relayeventtosapplication({
		    // 	Type: 'MessageReadEvent',
		    // 	Data: {
		    // 		MessageId: message.Id,
		    // 		DateTime: new Date()
		    // 	}
		    // });
		    // message.IsRead = true;
	    	$timeout(function () {
				if (conversation.IsOpen && message.IsOpen) { // only mark it as read if it is still open after 2 seconds
			    	Message.markasread({ messageIdentifier: message.Id })
					    .$promise.then(function(a) {
						    message.IsRead = true;
					    });
			    }
		    }, 5000);
	    };

	    $scope.MarkAsDone = function (conversation) {
			conversation.State = "markingasdone";
		    Message.markasdone({ conversationId: conversation.Id })
			    .$promise.then(function (a) {
					for ( var key in conversation.Messages)
					{
						conversation.Messages[key].IsReady = true;
					}
			    	// rather than removing it completely, just mark it as hidden, when we don't show it to the user, but we can merge it successfully with any updates that come down the line
			    	conversation.State = "done";
			    });
		    // $scope.hub.relayeventtosapplication({
		    // 	Type: 'MessageDoneEvent',
		    // 	Data: {
		    // 		MessageId: message.Id,
		    // 		DateTime: new Date()
		    // 	}
		    // });
		    return false;
	    };

	    function Update() {
	    	return Outbox.get({ term: window.folderName}).$promise
			.then(function (a) {
				a.forEach(function(v, i) {
					$scope.Views['outbox'].push(new Conversation(v));
				});
				$scope.Loading = false;
		    });
	    }
	    Update();
		
	    //function setUpdateTimeout() {
		   //  $timeout(function() {
			  //   Update()
				 //    .then(setUpdateTimeout,setUpdateTimeout);
		   //  }, 3000);
	    // }

	    function setVersionTimeout() {
		    $timeout(function() {
		    	$http.get("/version.txt")
					.success(function (v) {
						if ($scope.CurrentVersion == undefined) {
							$scope.CurrentVersion = v;
						} else {
							if ($scope.CurrentVersion != v) {
								location.reload();
							}
						}
				    setVersionTimeout();
			    }).error(setVersionTimeout);
		    }, 3000);
	    }

	    setVersionTimeout();
    }
})();
