function errorController($scope, $timeout) {
	    $scope.errors = [];
	    window.onerror = function (errorMsg, url, lineNumber) {
		    var error = {
			    IsNew: true,
			    Message: errorMsg,
			    Url: url,
			    LineNumber: lineNumber,
				Date: moment()
		    };
		    $scope.errors.push(error);
			$scope.$apply();
		    $timeout(function () {
			    error.IsNew = false;
		    }, 10000);
	    	return false;
	    };

	    $scope.closeAlert = function(index) {
		    $scope.alerts.splice(index, 1);
		};
    }

    angular
		.module('outboxApp')
		.controller('errorController', errorController);
