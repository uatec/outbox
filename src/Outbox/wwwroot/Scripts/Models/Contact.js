'use strict';

var Contact = function(json)
{
	var self = this;
	this.Email = json.Email || json.Name;
	this.Name = json.Name || json.Email;
	var gravatarHash = calcMD5(this.Email.trim().toLowerCase());
	var gravatarUrl = "//www.gravatar.com/avatar/" + gravatarHash + "?d=404";
	var request = new XMLHttpRequest();  
	request.open('GET', gravatarUrl, true);
	request.onreadystatechange = function(){
	    if (request.readyState === 4){
	        if (request.status === 200) {
				self.AvatarType = "gravatar";
				self.AvatarUrl = gravatarUrl;		
			}  
	    }
	};
	request.send();
	this.AvatarType = "initialicon";
	this.AvatarUrl = "//ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/avatars/avatar_tile_" + this.Name.substr(0, 1).toLowerCase() + "_28.png";
};