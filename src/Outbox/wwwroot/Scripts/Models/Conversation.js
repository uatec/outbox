
'use strict';


var Conversation = function(json)
{
	this.Id = json.Id;
	this.MessageIds = json.MessageIds || [];
	this.Subject = json.Subject;
	this.UnreadCount = function() {
		var unreadCount = 0;
		if ( Object.keys(this.Messages).length == 0 )
		{
			return json.UnreadCount;
		}
		for ( var key in this.Messages )
		{
			if ( !this.Messages[key].IsRead ) 
			{
				unreadCount++;
			}
		}
		return unreadCount;
	};
	// Dynamically populated hash of IDs (from MessageIds -> Message instance)
	this.Messages = {};
	this.State = "stubloaded";
	this.LatestDate = new moment(json.LatestDate);
	this.AddMessage = function(json)
	{
		this.Messages[json.Id] = new Message(json, this);	
	};
	this.AllMessagesLoaded = function()
	{
		return Object.keys(this.Messages).length == this.MessageIds.length;
	};
};
