'use strict';

var Message = function(json, parent)
{
	var self = this;
	this.Id = json.Id;
	this.To = [];
	(json.To || []).unique(function(v) { return v.Email; }).forEach(function(addr) {
		self.To.push(new Contact(addr));
	});
	this.CC = [];
	(json.CC || []).unique(function(v) { return v.Email; }).forEach(function(addr) {
		self.CC.push(new Contact(addr));
	});
	this.Attachments = json.Attachments;
	this.IsRead = json.IsRead;
	this.IsOpen = json.IsOpen;
	this.Draft = json.Draft || false;
	this.From = new Contact(json.From);
	this.Received = new moment(json.Received);
	this.SortOrder = this.Received.toISOString();
	this.ReplyTo = json.ReplyTo;
	this.Body = json.Body;
	this.GetParent = function() { return parent; };
	this.Type = json.Type;
};
