using System.Collections.Generic;
using Outbox.Models;
using System;

namespace Outbox.Data
{
	public interface IRequestManager
	{
		IDictionary<string, RequestInfo> GetAll();
		void RecordRequestStarting(
			DateTime requestStarted,
			string requestId, 
			string url, 
			string method, 
			string controller, 
			string action, 
			string sessionId, 
			string clientAddress);
		void RecordRequestEnded(string requestId);
	}
}

