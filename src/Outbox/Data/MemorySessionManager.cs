using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using Outbox.Models;

namespace Outbox.Data
{
	public class MemorySessionManager : ISessionManager
	{
		private readonly ConcurrentDictionary<string, Session> _sessions = 
			new ConcurrentDictionary<string, Session>();

		public IEnumerable<Session> GetAll()
		{
			return _sessions.Values;
		}

		public void RecordSessionActivity(string sessionId, string clientHostName, string userId)
		{
			Session session = _sessions.GetOrAdd(sessionId, new Session {
					SessionId = sessionId,
					ClientHostName = clientHostName,
					UserId = userId,
					SessionStarted = DateTime.Now,
					LastActive = DateTime.Now
				});
			session.UserId = userId;
			session.LastActive = DateTime.Now;
		}
	}
}