using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using Outbox.Models;

namespace Outbox.Data
{
	public class MemoryRequestManager : IRequestManager
	{
		private readonly ConcurrentDictionary<string, RequestInfo> _requests = 
			new ConcurrentDictionary<string, RequestInfo>();

		public IDictionary<string, RequestInfo> GetAll()
		{
			return _requests;
		}

		public void RecordRequestStarting(DateTime requestStarted, 
			string requestId, 
			string url, 
			string method, 
			string controller,
			string action,
			string sessionId, 
			string clientAddress)
		{
			_requests[requestId] = new RequestInfo 
			{
				RequestId = requestId,
				Url = url,
				Method = method,
				Controller = controller,
				Action = action,
				SessionId = sessionId,
				ClientAddress = clientAddress,
				RequestStarted = requestStarted
			};
		}

		public void RecordRequestEnded(string requestId)
		{
			RequestInfo goat;
			_requests.TryRemove(requestId, out goat);
		}
	}
}