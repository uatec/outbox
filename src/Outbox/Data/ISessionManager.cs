using System.Collections.Generic;
using Outbox.Models;

namespace Outbox.Data
{
	public interface ISessionManager
	{
		IEnumerable<Session> GetAll();
		void RecordSessionActivity(string sessionId, string clientHostName, string userId);
	}
}

