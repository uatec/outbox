﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Outbox
{
    public class MemoryBus : IBus
    {
        private List<Action<object>> _subscribers 
            = new List<Action<object>>();

        public void Publish<T>(T @event)
        {
            Task.Run(() => 
                Parallel.ForEach(_subscribers, s => s(@event)));
        }

        public void Subscribe<T>(Action<T> action)
        {
            _subscribers.Add(o => {
                if ( o is T )
                {
                    try 
                    {
                        action((T) o);
                    } 
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine("Unhandled exception thrown by subscriber.");
                        Console.Error.WriteLine("\tMessage: " + ex.Message);
                        Console.Error.WriteLine("\tStacktrace: " + ex.StackTrace);
                    }
                }
            });
        }
    }
}
