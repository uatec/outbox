FROM microsoft/aspnet:1.0.0-beta4

RUN curl -sL https://deb.nodesource.com/setup | bash -
RUN apt-get install -y nodejs adduser git
RUN npm install -g grunt-cli
ADD . /app

RUN chmod -R 777 /app
RUN adduser node --gecos "" --disabled-password

USER node

WORKDIR /app/src/Outbox

ENV MONO_MANAGED_WATCHER false
ENV ASPNET_TEMP /tmp
	

RUN dnu restore



EXPOSE 5004
ENTRYPOINT ["dnx", ".", "kestrel"]
